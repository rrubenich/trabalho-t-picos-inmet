class WelcomeController < ApplicationController
  def index
  end

  def charts
  end

  def reports
    @stations = Station.all

    respond_to do |format|
      format.html
      format.json
      format.pdf do
        pdf = StationPdf.new(@stations)
        send_data pdf.render, filename: "relatorio_estacoes.pdf",
        type: "application/pdf",
        disposition: "inline"
      end
    end
  end
end

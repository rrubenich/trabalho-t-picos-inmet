class LogsController < ApplicationController
  
  def index
    @logs = Log.all
  end

  def show
  end

  def new
    @log = Log.new
  end

  def edit
  end

  def create
    @station = Station.find(params[:station_id])
    @log = @station.logs.create!(log_params)
    
    respond_to do |format|
      format.html { redirect_to @station, notice: 'Leitura criada com sucesso.' }
      format.json { render :show, status: :created, location: @station }
    end
  end

  def update
    @station = Station.find(params[:station_id])
    @log = @station.logs.find(params[:id])


    respond_to do |format|
      if @log.update(log_params)
        format.html { redirect_to @station, notice: 'Leitura editada com sucesso.' }
        format.json { render :show, status: :ok, location: @station }
      else
        format.html { render :edit }
        format.json { render json: @station.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @station = Station.find(params[:station_id])
    @log = @station.logs.find(params[:id])

    @log.destroy

    respond_to do |format|
      format.html { redirect_to @station, notice: 'Leitura deletada com sucesso.' }
      format.json { head :no_content }
    end
  end

  def log_params
    params.require(:log).permit(:datetime, :inst_temperature, :max_temperature, :min_temperature, :inst_humidity, :max_humidity, :min_temperature, :inst_dew, :max_dew, :min_dew, :max_pressure, :min_pressure, :inst_pressure, :wind_direction, :wind_speed, :wind_gust, :radiation, :precipitation)
  end
end

class Station
  include Mongoid::Document
  field :station_code
  field :omm_code
  field :opening, type: Date
  field :latitude
  field :longitude
  field :city
  field :state

  validates_presence_of :station_code, :omm_code, :city

  embeds_many :logs

  field :_id, type: String, default: ->{ station_code }
end

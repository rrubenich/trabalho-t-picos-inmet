class Log
  include Mongoid::Document
  field :datetime, type: Time
  field :inst_temperature
  field :max_temperature
  field :min_temperature
  field :inst_humidity
  field :max_humidity
  field :min_temperature
  field :inst_dew
  field :max_dew
  field :min_dew
  field :max_pressure
  field :min_pressure
  field :inst_pressure
  field :wind_direction
  field :wind_speed
  field :wind_gust
  field :radiation
  field :precipitation


  embedded_in :station, inverse_of: :logs

  # field :_id, type: String, default: ->{ datetime }
end

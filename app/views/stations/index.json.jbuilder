json.array!(@stations) do |station|
  json.extract! station, :station_code, :omm_code, :opening, :latitude, :longitude, :city, :state, :logs
  json.url station_url(station, format: :json)
end

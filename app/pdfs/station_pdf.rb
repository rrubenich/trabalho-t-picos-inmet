class StationPdf < Prawn::Document
  def initialize(stations)
    super(top_margin: 70)
    @stations = stations
    header
    line_items
  end

  def header
    text "Todas Estações", size: 18
  end

  def line_items
    move_down 20
    horizontal_rule
    table line_items_rows do 
      row(0).font_style = :bold
      self.row_colors = ["FFFFFF","C9C9C9"]
    end
  end

  def line_items_rows
    [["Código", "OMM", "Abertura", "Latitude", "Longitude", "Cidade", "Estado" ]] +
    @stations.map do |item|
      [item.station_code, item.omm_code, item.opening.strftime("%m/%d/%Y "), item.latitude, item.longitude, item.city, item.state]
    end
  end
end
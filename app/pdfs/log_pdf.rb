class LogPdf < Prawn::Document
  def initialize(station)
    super(top_margin: 70)
    @station = station
    station_name
    line_items
  end

  def station_name
    text "Estação #{@station.city}", size: 18
  end

  def line_items
    move_down 20
    horizontal_rule
    table line_items_rows do 
      row(0).font_style = :bold
      self.row_colors = ["FFFFFF","C9C9C9"]
    end
  end

  def line_items_rows
    [["Data e Hora", "Temperatura", "Umidade", "Pressão", "Orvalho", "Precipitação (%)", "Radiação" ]] +
    @station.logs.map do |item|
      t = item.datetime
      datetime = t.strftime("%m/%d/%Y - %I:%M")
      [datetime, item.inst_temperature, item.inst_humidity, item.inst_pressure, item.inst_dew, item.precipitation, item.radiation]
    end
  end
end
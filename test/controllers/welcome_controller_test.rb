require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get charts" do
    get :charts
    assert_response :success
  end

  test "should get reports" do
    get :reports
    assert_response :success
  end

end
